-- +goose Up
-- +goose StatementBegin
create table news_part_1
(
    check ( category_id between 1 and 5 )
) inherits (news);

create table news_part_2
(
    check ( category_id > 5 )
) inherits (news);

-- RULES 

-- CREATE RULE news_insert_to_part_1 AS ON INSERT TO news
--     WHERE (category_id BETWEEN 1 AND 5)
--     DO INSTEAD INSERT INTO news_part_1
--                VALUES (new.*);
-- 
-- CREATE RULE news_insert_to_part_2 AS ON INSERT TO news
--     WHERE (category_id > 5)
--     DO INSTEAD INSERT INTO news_part_2
--                VALUES (new.*);

-- +goose StatementEnd

-- +goose Down
drop table news_part_1;
-- +goose StatementBegin
-- +goose StatementEnd
