-- +goose Up
-- +goose StatementBegin
INSERT INTO news (category_id, title, author, rate)
values (1, 'News_1', 'John', 1),
       (2, 'News_2', 'Paul', 2),
       (2, 'News_3', 'George', 3),
       (6, 'News_4', 'Ringo', 4),
       (7, 'News_5', 'Huston', 5);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
TRUNCATE news RESTART IDENTITY CASCADE;
-- +goose StatementEnd
