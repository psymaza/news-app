module news_app

go 1.21

require (
	github.com/go-chi/chi v1.5.5
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.9
	github.com/pressly/goose/v3 v3.16.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/Masterminds/squirrel v1.5.4 // indirect
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/sethvargo/go-retry v0.2.4 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/sync v0.5.0 // indirect
)
