FROM alpine

WORKDIR /app

COPY --from=build:develop /app/cmd/main ./app

CMD ["/app/app", "-c", "config.yaml"]