package http

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"

	"news_app/internal/model"
	"news_app/internal/repo"
)

func getParams(v url.Values) *repo.Params {
	params := &repo.Params{
		Author: v.Get("author"),
	}

	if lim := v.Get("limit"); lim != "" {
		params.Limit, _ = strconv.ParseUint(lim, 10, 0)
	}

	if offset := v.Get("offset"); offset != "" {
		params.Offset, _ = strconv.ParseUint(offset, 10, 0)
	}

	return params
}

func (a *Server) GetHandler(w http.ResponseWriter, r *http.Request) {
	var (
		ctx    = r.Context()
		params = getParams(r.URL.Query())
		news   []model.News
		err    error
	)

	news, err = a.newsService.Get(ctx, params)

	if err != nil {
		writeError(w, err)
		return
	}

	writeJSONResponse(w, http.StatusOK, news)
}

func (a *Server) CreateHandler(w http.ResponseWriter, r *http.Request) {
	var (
		ctx  = r.Context()
		news = &model.News{}
	)

	if err := json.NewDecoder(r.Body).Decode(news); err != nil {
		writeError(w, err)
		return
	}

	err := a.newsService.Create(ctx, news)
	if err != nil {
		writeError(w, err)
		return
	}

	writeJSONResponse(w, http.StatusCreated, "OK")
}
