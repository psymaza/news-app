package http

type Config struct {
	BasePath     string `yaml:"base_path"`
	ServeAddress string `yaml:"serve_address"`
}
