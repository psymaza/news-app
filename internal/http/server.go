package http

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"

	"news_app/internal/services/newssvc"
)

var ErrServerClosed = http.ErrServerClosed

type Server struct {
	config *Config

	newsService *newssvc.NewsService

	server *http.Server
}

func (a *Server) Serve() error {
	r := chi.NewRouter()

	apiRouter := chi.NewRouter()
	apiRouter.Get("/news", a.GetHandler)
	apiRouter.Post("/news", a.CreateHandler)

	r.Mount(a.config.BasePath, apiRouter)

	a.server = &http.Server{Addr: a.config.ServeAddress, Handler: r}

	return a.server.ListenAndServe()
}

func (a *Server) Shutdown(ctx context.Context) {
	_ = a.server.Shutdown(ctx)
}

func New(config *Config, newsService *newssvc.NewsService) *Server {
	return &Server{
		config:      config,
		newsService: newsService,
	}
}
