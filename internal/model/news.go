package model

type News struct {
	ID         int    `json:"id" db:"id"`
	CategoryID int    `json:"categoryId"  db:"category_id"`
	Rate       int    `json:"rate" db:"rate"`
	Author     string `json:"author" db:"author"`
	Title      string `json:"title" db:"title"`
}
