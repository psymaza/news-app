package repo

import (
	"context"
	"database/sql"
	"errors"

	"github.com/jmoiron/sqlx"

	sq "github.com/Masterminds/squirrel"

	"news_app/internal/model"
)

type Params struct {
	Title  string
	Author string
	Limit  uint64
	Offset uint64
}

type NewsRepo struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) *NewsRepo {
	return &NewsRepo{db: db}
}

func (r *NewsRepo) WithNewTx(ctx context.Context, opts *sql.TxOptions, f func(ctx context.Context) error) error {
	tx, err := r.db.BeginTx(ctx, opts)
	if err != nil {
		return err
	}

	defer func() {
		rollbackErr := tx.Rollback()
		if rollbackErr != nil && !errors.Is(rollbackErr, sql.ErrTxDone) {
			err = rollbackErr
		}
	}()

	fErr := f(ctx)
	if fErr != nil {
		_ = tx.Rollback()
		return fErr
	}

	return tx.Commit()
}

//
// func (r *NewsRepo) Get(ctx context.Context, params *Params) ([]model.News, error) {
// 	query := "select * from news"
//
// 	if params.Author != "" {
// 		// query = fmt.Sprintf("%s where author = '%s'", query, params.Author)
// 		query = fmt.Sprintf("%s where author = $1", query)
// 	}
//
// 	rows, err := r.db.QueryxContext(ctx, query, "1", "", "")
// 	if err != nil {
// 		return nil, err
// 	}
//
// 	defer rows.Close()
//
// 	news := make([]model.News, 0)
//
// 	for rows.Next() {
// 		n := model.News{}
//
// 		if err = rows.StructScan(&n); err != nil {
// 			return nil, err
// 		}
//
// 		news = append(news, n)
// 	}
//
// 	return news, nil
// }

func (r *NewsRepo) Get(ctx context.Context, params *Params) ([]model.News, error) {
	query := sq.Select("*").
		From("news").
		Offset(params.Offset).
		Limit(params.Limit).
		PlaceholderFormat(sq.Dollar)

	if params.Author != "" {
		query = query.Where(sq.Eq{"author": params.Author})
	}

	if params.Title != "" {
		query = query.Where(sq.Eq{"title": params.Title})
	}

	sql, args, err := query.ToSql()
	rows, err := r.db.QueryxContext(ctx, sql, args...)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	news := make([]model.News, 0)

	for rows.Next() {
		n := model.News{}

		if err = rows.StructScan(&n); err != nil {
			return nil, err
		}

		news = append(news, n)
	}

	return news, nil
}

func getTableName(n *model.News) string {
	if n.CategoryID < 5 {
		return "news_part_1"
	} else {
		return "news_part_2"
	}

	return ""
}

func (r *NewsRepo) Create(ctx context.Context, n *model.News) error {
	sql, args, err := sq.
		Insert(getTableName(n)).Columns("title", "author", "category_id", "rate").
		Values(n.Title, n.Author, n.CategoryID, n.Rate).
		Suffix("RETURNING id").
		PlaceholderFormat(sq.Dollar).
		ToSql()
	if err != nil {
		return err
	}

	// if _, err = r.db.ExecContext(ctx, sql, args...); err != nil {
	// 	return err
	// }

	var id int
	row := r.db.QueryRowContext(ctx, sql, args...)
	if err = row.Scan(&id); err != nil {
		return err
	}

	return nil
}
