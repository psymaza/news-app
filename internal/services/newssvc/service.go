package newssvc

import (
	"context"
	"database/sql"

	"news_app/internal/model"
	"news_app/internal/repo"
)

type NewsService struct {
	rep *repo.NewsRepo
}

func New(rep *repo.NewsRepo) *NewsService {
	return &NewsService{rep: rep}
}

func (s *NewsService) Get(ctx context.Context, params *repo.Params) ([]model.News, error) {
	return s.rep.Get(ctx, params)
}

func (s *NewsService) Create(ctx context.Context, n *model.News) error {
	return s.rep.WithNewTx(ctx, &sql.TxOptions{Isolation: sql.LevelDefault}, func(ctx context.Context) error {
		return s.rep.Create(ctx, n)
	})
}
