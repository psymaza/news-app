package app

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pressly/goose/v3"

	"news_app/internal/http"
	"news_app/internal/repo"
	"news_app/internal/services/newssvc"
)

const driverName = "postgres"

type App struct {
	config *Config
	db     *sqlx.DB

	httpServer *http.Server
}

func (a *App) Serve() error {
	done := make(chan os.Signal, 1)

	signal.Notify(done, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		if err := a.httpServer.Serve(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatal(err.Error())
		}
	}()

	<-done

	a.Shutdown()

	return nil
}

func (a *App) Shutdown() {
	ctx, cancel := context.WithTimeout(context.Background(), a.config.App.ShutdownTimeout)
	defer cancel()

	a.httpServer.Shutdown(ctx)
}

func New(config *Config) (*App, error) {
	db, err := initDB(context.Background(), &config.Database)
	if err != nil {
		return nil, err
	}

	newsRepo := repo.New(db)
	newsService := newssvc.New(newsRepo)

	a := &App{
		config:     config,
		db:         db,
		httpServer: http.New(&config.HTTP, newsService),
	}

	return a, nil
}

func initDB(ctx context.Context, config *DatabaseConfig) (*sqlx.DB, error) {
	db, err := sqlx.Open(driverName, config.DSN)
	if err != nil {
		return nil, fmt.Errorf("unable to connect to database: %w", err)
	}

	db.DB.SetMaxOpenConns(100)  // The default is 0 (unlimited)
	db.DB.SetMaxIdleConns(10)   // defaultMaxIdleConns = 2
	db.DB.SetConnMaxLifetime(0) // 0, connections are reused forever.

	if err = db.PingContext(ctx); err != nil {
		return nil, err
	}

	// migrations

	fs := os.DirFS(config.MigrationsDir)
	goose.SetBaseFS(fs)

	if err = goose.SetDialect(driverName); err != nil {
		panic(err)
	}

	if err = goose.UpContext(ctx, db.DB, "."); err != nil {
		panic(err)
	}

	return db, nil
}
