package app

import (
	"os"
	"time"

	"gopkg.in/yaml.v3"

	"news_app/internal/http"
)

const (
	AppName                     = "auth"
	DefaultServeAddress         = "localhost:9626"
	DefaultShutdownTimeout      = 20 * time.Second
	DefaultBasePath             = "/auth/v1"
	DefaultAccessTokenCookie    = "access_token"
	DefaultRefreshTokenCookie   = "refresh_token"
	DefaultSigningKey           = "qwerty"
	DefaultAccessTokenDuration  = 1 * time.Minute
	DefaultRefreshTokenDuration = 1 * time.Hour
	DefaultGRPCPort             = 9909
	DefaultDSN                  = "dsn://"
	DefaultMigrationsDir        = "file://migrations/auth"
)

type AppConfig struct {
	Debug           bool          `yaml:"debug"`
	ShutdownTimeout time.Duration `yaml:"shutdown_timeout"`
}

type DatabaseConfig struct {
	DSN           string `yaml:"dsn"`
	MigrationsDir string `yaml:"migrations_dir"`
}

type Config struct {
	App      AppConfig      `yaml:"app"`
	HTTP     http.Config    `yaml:"http"`
	Database DatabaseConfig `yaml:"database"`

	// Auth service.AuthConfig `yaml:"auth"`
}

func NewConfig(fileName string) (*Config, error) {
	data, err := os.ReadFile(fileName)
	if err != nil {
		return nil, err
	}

	cnf := Config{
		App: AppConfig{
			ShutdownTimeout: DefaultShutdownTimeout,
		},
		Database: DatabaseConfig{
			DSN:           DefaultDSN,
			MigrationsDir: DefaultMigrationsDir,
		},
	}

	if err = yaml.Unmarshal(data, &cnf); err != nil {
		return nil, err
	}

	return &cnf, nil
}
